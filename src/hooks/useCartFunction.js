import { useContext, useEffect, useState } from 'react';
import axios from 'axios';
import { baseUrl, } from '../utils/api';
import { AuthContext } from '../context/AuthContext';
import { CartContext } from '../context/CartContext';
import Swal from 'sweetalert2';

export const useCartFunction = () => {

    const { token, AuthLogout } = useContext(AuthContext)

    const { CountCart } = useContext(CartContext)

    const [isLoading, setLoading] = useState(false)
    const [isListLoading, setIsListLoading] = useState(false);
    const [listCart, setListCart] = useState([]);
    const [isDataChange, setIsDataChange] = useState(true);
    const [isAllChecked, setIsAllChecked] = useState(false);
    const [proceedPayment, setProceedPayment] = useState(false);
    const [totalPrice, setTotalPrice] = useState(0);

    const getDataCart = async () => {
        try {
            setIsListLoading(true)
            const response = await axios.get(`${baseUrl}Cart`,
                { headers: { 'Authorization': `Bearer ${token}` } });
            setListCart(response.data)
            CountCart(response.data)
            if (response.data && response.data.length > 0) {
                const checkPrice = response.data.reduce((acc, cartItem) => {
                    if (cartItem.isSelected) { return acc + cartItem.price }
                    else return acc
                }, 0);
                setTotalPrice(checkPrice)
                const checkAll = response.data.every((cart) => cart.isSelected)
                const atLeastOneSelected = response.data.some((cart) => cart.isSelected);
                setProceedPayment(atLeastOneSelected)
                setIsAllChecked(checkAll)
            }

        } catch (error) {
            console.error('Error fetching data:', error);
        } finally {
            setIsListLoading(false)
        }
    }

    useEffect(() => {
        if (isDataChange) {
            getDataCart();
            setIsDataChange(false)
        }
    }, [isDataChange])



    const addToCart = async (idSchedule) => {
        setLoading(true)
        console.log(idSchedule)

        try {
            await axios.post(
                baseUrl + "Cart",
                {
                    "scheduleId": idSchedule
                },
                { headers: { 'Authorization': `Bearer ${token}` }, }
            ).then(() => {
                setIsDataChange(true)
                Swal.fire({
                    title: "Berhasil!",
                    text: "Course telah dimasukkan ke keranjang",
                    icon: "success"
                });
            })

        } catch (error) {

            if (error.response && error.response.status === 401) {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Unauthorized access. Please log in again.",
                });
                AuthLogout();
            } else if (error.response && error.response.status === 400) {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: `${error.response.data.status}`,
                });
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Gagal menambahkan. Hubungi CS",
                });
            }
        }

        setLoading(false)
    }

    const handleSelectCart = async (cartId) => {
        setLoading(true)
        const cartItem = listCart.find(item => item.id === cartId);
        const changedValue = !cartItem.isSelected
        try {
            await axios.put(
                baseUrl + "Cart/cartSelected/" + cartId,
                changedValue,
                {
                    headers: {
                        'Authorization': `Bearer ${token}`,
                        'Content-Type': 'application/json',
                    },
                }
            ).then(() => {
                setIsDataChange(true);
            })
        }
        catch (error) {

            if (error.response && error.response.status === 401) {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Unauthorized access. Please log in again.",
                });
                AuthLogout();
            } else if (error.response && error.response.status === 400) {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: `${error.response.data.status}`,
                });
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Gagal menambahkan. Hubungi CS",
                });
            }
        }
        setLoading(false)
    }

    const handleSelectAllCart = async () => {
        setLoading(true)
        const updateValue = !isAllChecked
        try {
            await axios.put(
                baseUrl + "Cart/selectAll",
                updateValue,
                {
                    headers: {
                        'Authorization': `Bearer ${token}`,
                        'Content-Type': 'application/json',
                    },
                }
            ).then(() => {
                setIsDataChange(true);
            })
        }
        catch (error) {

            if (error.response && error.response.status === 401) {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Unauthorized access. Please log in again!",
                });
                AuthLogout();
            } else if (error.response && error.response.status === 400) {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: `${error.response.data.status}`,
                });
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Gagal menambahkan. Hubungi CS",
                });
            }
        }
        setLoading(false)
    }

    const handleDeleteCart = async (cartId) => {
        setLoading(true)
        try {
            await axios.delete(
                baseUrl + "Cart/" + cartId,
                { headers: { 'Authorization': `Bearer ${token}` }, }
            ).then(() => {
                setIsDataChange(true);
                console.log("delete success");
            })
        }
        catch (error) {

            if (error.response && error.response.status === 401) {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Unauthorized access. Please log in again.",
                });
                AuthLogout();
            } else if (error.response && error.response.status === 400) {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: `${error.response.data.status}`,
                });
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Gagal menambahkan. Hubungi CS",
                });
            }
        }
        setLoading(false)
    }



    return {
        isListLoading,
        listCart,
        proceedPayment,
        addToCart,
        handleSelectCart,
        handleSelectAllCart,
        handleDeleteCart,
        isAllChecked,
        totalPrice,
        isLoading,
    }
}
