import { Box, Container, Stack, TextField } from "@mui/material";
import Typography from '@mui/material/Typography';
import { Link } from "react-router-dom";
import { blackColor, blueColor, gray1Color, gray2Color } from "../utils/color";
import FlatButton from "../components/button/FlatButton";
import { useEffect, useState } from 'react';
import { useAuth } from "../hooks/useAuth";

export const LoginPage = ({ pathCheck }) => {
  // const navigate = useNavigate();
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const { login, isLoading } = useAuth();

  const submitLogin = async (e) => {
    e.preventDefault()
    console.log(email + password)
    // setLogin(true);
    if (!isLoading) {
      login(email, password, pathCheck)
    }
    // if (pathCheck){
    //   navigate('/');
    // } else{
    //   // navigate('/');
    //   navigate(-1);
    // }
  }

  return (
    <Container maxWidth="sm">
      <Stack
        // justifyContent={'center'}
        // alignItems={'center'}
        // height={`calc(100vh - 68px)`}
        // marginTop={'182px'}
        sx={{
          display: "flex",
          flexDirection: "column",
          marginTop: '182px',
          gap: "40px",
        }}
      >
        <Box sx={{
          display: "flex",
          justifyContent: "start",
          flexDirection: "column",
          gap: "60px",
        }}>
          <Box sx={{
            display: "flex",
            justifyContent: "start",
            flexDirection: "column",
            gap: "16px",
          }}>
            <Typography color={gray1Color} variant="h5">
              Selamat Datang Musikers!
            </Typography>
            <Typography variant="subtitle1" color={gray2Color}>
              Login dulu yuk
            </Typography>
          </Box>

          <form autoComplete="off" onSubmit={submitLogin} style={{
            display: "flex",
            flexDirection: "column",
            gap: "24px",
          }}>
            <TextField
              fullWidth
              required
              id="email"
              label="Masukan Email"
              variant="outlined"
              margin="none"
              size="small"
              type="text"
              onChange={e => setEmail(e.target.value)}
              value={email}

            />
            <TextField
              fullWidth
              required
              id="password"
              label="Masukan Password"
              variant="outlined"
              margin="none"
              type="password"
              size="small"
              onChange={e => setPassword(e.target.value)}
              value={password}

            />
            <Link style={{ textDecoration: 'none' }} to={'/forgot-password'}>
              <Typography variant="caption"
                display="flex"
                gutterBottom
                justifyContent={"flex-end"}
                color={gray2Color}>
                Lupa kata sandi
              </Typography>
            </Link>

            <Box sx={{
              display: "flex",
              justifyContent: "start",
              flexDirection: "column",
              gap: "24px",
            }}
            >
              <FlatButton title={isLoading ? "Loading" : "Masuk"} />
              <Typography variant="caption" display="flex" color={blackColor} >
                Belum punya akun?
                <Link style={{ textDecoration: 'none' }} to={'/register'}>
                  <Typography variant="caption" color={blueColor} >
                    Daftar disini
                  </Typography>
                </Link>
              </Typography>
            </Box>
          </form>
        </Box>
      </Stack>
    </Container>
  )
}
