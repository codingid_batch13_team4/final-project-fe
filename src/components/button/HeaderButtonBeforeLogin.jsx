import { Box, Button } from "@mui/material"
import { Link } from "react-router-dom"
import { blackColor, irisColor, whiteColor } from "../../utils/color"

export const HeaderBeforeLogin = () => {
    return(
        <Box height={'44px'}sx={{display: "flex",
        flexDirection: "row",
        gap: "40px",
        }}>
            <Link to='/register'>
                <Button 
                style={{
                    color:blackColor(),
                    height:'100%',
                    textTransform: 'none',
                    fontSize: '16px',
                    fontWeight: 500,
                    textAlign: 'left'
                }}>
                    Daftar Sekarang
                </Button>
            </Link>

            <Link to='/login'>
                <Button
                variant="text"
                style={{
                    background: irisColor(),
                    color: whiteColor(),
                    marginRight:'0px',
                    padding:'10px 20px',
                    borderRadius: '8px',
                    width: '93px',
                    height:'100%',
                    fontSize: '16px',
                    fontWeight: 500,
                    textTransform: 'none'
                }}>
                    Masuk
                </Button>
            </Link>
        </Box>
    )
}