import { Badge, Box, Button } from "@mui/material";
import { blackColor, cartColor, gray1Color, irisColor, whiteColor } from "../../utils/color";
import { Link, useNavigate } from "react-router-dom";
import { Logout, Person, ShoppingCart } from "@mui/icons-material";
import { AuthContext } from "../../context/AuthContext";
import { useContext } from "react";
import { CartContext } from "../../context/CartContext";

export const HeaderAfterLogin = () => {
    const navigate = useNavigate();
    const { AuthLogout } = useContext(AuthContext)
    const { cartAmount } = useContext(CartContext)
    const textButtonStyle = {
        display: 'flex',
        color: blackColor(),
        padding: '6px 10px',
        margin: '0px 0px 0px 20px',
        borderRadius: '12px',
        fontSize: '16px',
        textTransform: 'none'
    }
    const handleLogout = () => {
        AuthLogout()
        navigate('/login')
    }

    return (
        <Box sx={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: "center"
        }}>
            <Link to='/checkout'>
                <Badge badgeContent={cartAmount} color={cartAmount != 0 ? "primary": "default"}>
                    <ShoppingCart sx={{ color: cartColor() }} />
                </Badge>
            </Link>
            <Link to='/my-class' style={{ textDecoration: 'none' }}>
                <Button variant="text" sx={textButtonStyle} >
                    Kelasku
                </Button>
            </Link>
            <Link to='/invoice' style={{ textDecoration: 'none' }}>
                <Button variant="text" sx={textButtonStyle}>
                    Pembelian
                </Button>
            </Link>
            <Box sx={{
                width: '1px',
                height: 24,
                backgroundColor: blackColor(),
                margin: "0px 34px 0px 24px"
            }} />
            <Person sx={{ color: gray1Color() }} />
            <div onClick={handleLogout}>
                <Logout sx={{ color: gray1Color(), margin: "0px 0px 0px 24px", cursor: "pointer" }} />
            </div>
        </Box>
    )
};