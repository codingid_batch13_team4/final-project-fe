import { Box, Container, Grid, Paper, Typography } from "@mui/material";
import concert from "../../assets/concert.jpg"
import { whiteColor } from "../../utils/color";

export const LandingImageThumbnail = () => {
    return (
        <Box
          sx={{
            background: `url(${concert}) center/cover no-repeat`, 
            color: 'white',
            
            // height: 'calc(100vh - 68px)',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            paddingY: '250px',
            fontFamily: 'Poppins, sans-serif',
            marginTop:'92px',
            filter: 'brightness(0.95)' 
          }}
        >
          <Container>
            <Grid container spacing={6} sx={{ marginBottom: '50px' }}>
              <Grid item lg={12} xs={12}>
                <Typography
                  variant="h3"
                  component="h4"
                  color="textPrimary"
                  style={{
                    fontWeight: '500',
                    color: whiteColor(),
                    textAlign: 'center',
                    textShadow: '0 0 4px #000',
                  }}
                >
                  Hi Musiker! Gabung yuk di Apel Music
                </Typography>
              </Grid>
    
              <Grid item lg={12} xs={12}>
                <Typography
                  variant="h6"
                  component="h6"
                  fontWeight={'400'}
                  color="textPrimary"
                  style={{ textAlign: 'center', color: whiteColor(), textShadow: '0 0 4px #000', }}
                >
                  Banyak kelas keren yang bisa menunjang bakat bermusik kamu
                </Typography>
              </Grid>
            </Grid>
    
            <Grid container spacing={6}>
              <Grid item lg={4} xs={12}>
                <Paper
                  elevation={12}
                  sx={{
                    width: '100%',
                    textAlign: 'center',
                    justifyContent: 'space-between',
                    padding: '48px',
                  }}
                >
                  <Typography
                    variant="h3"
                    component="h4"
                    color="secondary"
                    style={{ fontSize: '48px' }}
                  >
                    500+
                  </Typography>
                  <Typography
                    variant="body1"
                    component="h4"
                    color="textPrimary"
                    style={{ fontSize: '16px', marginTop: '20px' }}
                  >
                    Lebih dari sekedar kelas biasa yang bisa mengeluarkan bakat kalian
                  </Typography>
                </Paper>
              </Grid>
    
              <Grid item lg={4} xs={12}>
                <Paper
                  elevation={12}
                  sx={{ width: '100%', textAlign: 'center', padding: '60px' }}
                >
                  <Typography
                    variant="h3"
                    component="h4"
                    color="secondary"
                    style={{ fontSize: '48px' }}
                  >
                    50+
                  </Typography>
                  <Typography
                    variant="body1"
                    component="h4"
                    color="textPrimary"
                    style={{ fontSize: '16px', marginTop: '20px' }}
                  >
                    Lulusan yang menjadi musisi ternama dengan skill memukau
                  </Typography>
                </Paper>
              </Grid>
    
              <Grid item lg={4} xs={12}>
                <Paper
                  elevation={12}
                  sx={{ width: '100%', textAlign: 'center', padding: '60px' }}
                >
                  <Typography
                    variant="h3"
                    component="h4"
                    color="secondary"
                    style={{ fontSize: '48px' }}
                  >
                    10+
                  </Typography>
                  <Typography
                    variant="body1"
                    component="h4"
                    color="textPrimary"
                    style={{ fontSize: '16px', marginTop: '20px' }}
                  >
                    Coach Special kolaborasi dengan musisi terkenal
                  </Typography>
                </Paper>
              </Grid>
            </Grid>
          </Container>
        </Box>
      );
}