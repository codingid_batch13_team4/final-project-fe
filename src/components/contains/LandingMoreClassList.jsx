import { CircularProgress, Grid, Paper, Stack, Typography } from "@mui/material"
import PropTypes from 'prop-types';
import { Link } from "react-router-dom"
import { gray3Color, irisColor, whiteColor } from "../../utils/color"
import { imgUrl } from "../../utils/api";
import { ClassCard } from "../card/ClassCard";

export const LandingMoreClassList = (Props) => {

    if (!Props) {
        return (
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: '782px' }}>
                <CircularProgress />
            </div>
        );
    }

    return (
        <Stack justifyContent={'center'} alignItems={'center'} padding={{ xs: '0px 30px', md: '0px 91px' }} flex={1} style={{
            marginTop: '100px', backgroundColor: whiteColor()
        }}>
            {Props.otherList.length === 0 ? (
                <Typography variant="h5" style={{
                    textAlign: 'center',
                    margin: '0 auto',
                    fontWeight: '600',
                    fontSize: '24px',
                    color: irisColor(),
                    marginBottom: '100px'
                }}>
                    Tidak ada kelas lain yang tersedia
                </Typography>
            ) :
                (<>
                    <Typography ariant="h6" style={{
                        textAlign: 'center',
                        margin: '0 auto',
                        fontWeight: '600',
                        fontSize: '24px',
                        color: irisColor(),
                        marginBottom: '20px'
                    }}>
                        {Props.title}
                    </Typography>
                    <Grid container spacing={4}>
                        {Props.otherList.map(value => (
                            <Grid item xs={12} md={4} key={value.id}>
                                <Paper style={{
                                    margin: '10px',
                                    boxShadow: 'none',
                                    flexDirection: 'column',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    borderRadius: '16px'
                                }}>
                                    <ClassCard value={value} />
                                </Paper>
                            </Grid>
                        ))}
                    </Grid>
                </>)}
        </Stack>
    )
}

LandingMoreClassList.propTypes = {
    title: PropTypes.string.isRequired,
    // Add more PropTypes if needed
};