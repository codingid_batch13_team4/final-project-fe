import React, { useEffect, useState } from 'react';
import { ThemeProvider, createTheme, CssBaseline, Box } from '@mui/material';
import { EmailConfirmed } from './pages/EmailConfirmed';
import { LoginPage } from './pages/LoginPage';
import './App.css';
import { BrowserRouter, Route, Routes, useLocation, } from 'react-router-dom';
import { HeaderComp } from './components/header/HeaderComp';
import { ForgotPassword } from './pages/ForgotPassword';
import { RegisterPage } from './pages/RegisterPage';
import { NewPassPage } from './pages/NewPassPage';
import { LandingPage } from './pages/LandingPage';
import { DetailClass } from './pages/DetailClass';
import ScrollToTop from './utils/ScrollToTop';
import { ClassListPage } from './pages/ClassListPage';
import { CheckoutPage } from './pages/CheckoutPage';
import { MyClass } from './pages/Myclass';
import { PaymentConfirmed } from './pages/PaymentConfirmed';
import { DetailInvoice } from './pages/DetailInvoice';
import { InvoicePage } from './pages/InvoicePage';
import AdminLayout from './layout/AdminLayout';
import { AuthProvider } from './context/AuthContext';
import { AdminCoursePage } from './pages/admin/AdminCoursePage';
import { AdminAddCourse } from './pages/admin/AdminAddCourse';
import { AdminEditCourse } from './pages/admin/AdminEditCourse';
import { AdminCategoryPage } from './pages/admin/AdminCategoryPage';
// import { AdminAddCategory } from './pages/admin/AdminAddCategory';
import { AdminEditCategory } from './pages/admin/AdminEditCategory';
import { AdminUserPage } from './pages/admin/AdminUserPage';
import { AdminAddUser } from './pages/admin/AdminAddUser';
import { AdminEditUser } from './pages/admin/AdminEditUser';
import { AdminAddCategory } from './pages/admin/AdminAddCategory';
import { AdminPaymentPage } from './pages/admin/AdminPaymentPage';
import { AdminAddPayment } from './pages/admin/AdminAddPayment';
import { AdminEditPayment } from './pages/admin/AdminEditPayment';
import { AdminInvoicePage } from './pages/admin/AdminInvoicePage';
import { AdminDetailInvoice } from './pages/admin/AdminDetailInvoice';
import { AdminScheulePage } from './pages/admin/AdminSchedulePage';
import { AdminAddSchedule } from './pages/admin/AdminAddSchedule';
import { GuestMiddleware } from './middlewares/GuestMiddleware';
import { AuthMiddleware } from './middlewares/AuthMiddleware';
import { AdminMiddleware } from './middlewares/AdminMiddleware';
import { MainHeader } from './components/header/MainHeader';
import CartProvider from './context/CartContext';
import { LandingMiddleware } from './middlewares/LandingMiddleware';
import Dashboard from './pages/admin/Dashboard';

const theme = createTheme({
  typography: {
    fontFamily: 'Poppins, sans-serif',
  },
});

const PageTitleUpdater = () => {
  const location = useLocation();

  useEffect(() => {
    document.title = `Apple Music | ${location.pathname.slice(1) || 'Home'}`;
  }, [location.pathname]);

  return null;
};

function App() {
  const [pathCheck, setPathCheck] = useState(true);

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <AuthProvider>
        <CartProvider>
        <BrowserRouter>
        <PageTitleUpdater/>
          <Box>
            {/* <HeaderComp setPathCheck={setPathCheck} /> */}
            <MainHeader setPathCheck={setPathCheck} />
            <ScrollToTop />
            <Routes> 
            <Route path="/" exact element={<LandingMiddleware><LandingPage /></LandingMiddleware>} />
            <Route path="/login" exact element={<GuestMiddleware><LoginPage pathCheck={pathCheck} /></GuestMiddleware>} />
            <Route path="/register" exact element={<GuestMiddleware><RegisterPage /></GuestMiddleware>} />
            <Route path="/verify/*" exact element={<LandingMiddleware><EmailConfirmed /></LandingMiddleware>} />
            <Route path="/forgot-password" exact element={<GuestMiddleware><ForgotPassword /></GuestMiddleware>} />
            <Route path="/new-password/*" exact element={<GuestMiddleware><NewPassPage /></GuestMiddleware>} />
            <Route path="/detail-class/:id" exact element={<LandingMiddleware><DetailClass /></LandingMiddleware>} />
            <Route path="/list-menu/:id" element={<LandingMiddleware><ClassListPage /></LandingMiddleware>} />
            <Route path="/checkout" exact element={<AuthMiddleware><CheckoutPage /></AuthMiddleware>} />
            <Route path="/payment-confirmed" exact element={<LandingMiddleware><PaymentConfirmed /></LandingMiddleware>} />
            <Route path="/my-class" exact element={<AuthMiddleware><MyClass /></AuthMiddleware>} />
            <Route path="/invoice" exact element={<AuthMiddleware><InvoicePage /></AuthMiddleware>} />
            <Route path="/detail-invoice/:no" exact element={<AuthMiddleware><DetailInvoice /></AuthMiddleware>} />


            <Route path='/admin' exact element= {
                <AdminMiddleware><AdminLayout/></AdminMiddleware>
              }>
              <Route path='' exact element= {
                <Dashboard/>
              }/>
              <Route path='admin-course' exact element= {
                <AdminCoursePage/>
              }/>
              <Route path='add-course' exact element= {
                <AdminAddCourse/>
              }/>
              <Route path='edit-cource/:id' exact element= {
                <AdminEditCourse/>
              }/>
              <Route path='admin-category' exact element= {
                <AdminCategoryPage/>
              }/>
              <Route path='add-category' exact element= {
                <AdminAddCategory/>
              }/>
              <Route path='edit-category/:id' exact element= {
                <AdminEditCategory/>
              }/>
              <Route path='admin-user' exact element= {
                <AdminUserPage/>
              }/>
              <Route path='add-user' exact element= {
                <AdminAddUser/>
              }/>
              <Route path='edit-user/:id' exact element= {
                <AdminEditUser/>
              }/>
              <Route path='admin-payment' exact element= {
                <AdminPaymentPage/>
              }/>
              <Route path='add-payment' exact element= {
                <AdminAddPayment/>
              }/>
              <Route path='edit-payment/:id' exact element= {
                <AdminEditPayment/>
              }/>
              <Route path='admin-invoice' exact element= {
                <AdminInvoicePage/>
              }/>
              <Route path='admin-detail-invoice/:no' exact element= {
                <AdminDetailInvoice/>
              }/>
              <Route path='admin-schedule' exact element= {
                <AdminScheulePage/>
              }/>
              <Route path='add-schedule' exact element= {
                <AdminAddSchedule/>
              }/>
            </Route>
            </Routes>
          </Box>
        </BrowserRouter>
        </CartProvider>
      </AuthProvider>
    </ThemeProvider>
  );
}

export default App;
