import { useContext, useEffect, useState } from "react"
import { AuthContext } from "../context/AuthContext"
import axios from "axios"
import { baseUrl } from "../utils/api"
import Swal from "sweetalert2"

export const useAdminCategory = () => {
    // let baseUrl = import.meta.env.VITE_API_URL

    const { token, AuthLogout } = useContext(AuthContext)

    const [isLoading, setLoading] = useState(true)
    const [actionLoading, setActionLoading] = useState(false)
    const [data, setData] = useState([])

    const getAdminCategory = async () => {
        try {
            await axios.get(baseUrl + "Category",
                {
                    headers: { 'Authorization': `Bearer ${token}` }
                }).then(payload => {
                    console.log("ini payload", payload.data)
                    setData(payload.data)
                    setLoading(false)
                })
        } catch (error) {
            if (error.response && error.response.status === 401) {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Unauthorized access. Please log in again.",
                });
                AuthLogout();
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: `${error.response.data.status}`,
                });
                setLoading(false)
            }
        }
    }

    useEffect(() => {
        getAdminCategory()
    }, [])

    const updateStatus = async (status, id) => {
        setActionLoading(true)
        try {
            await axios.put(
                baseUrl + `Category?id=${id}&status=${status}`,
                {},
                {
                    headers: { 'Authorization': `Bearer ${token}` }
                }
            ).then(payload => {
                console.log(payload.data)
                getAdminCategory()
                setActionLoading(false)
            })
        } catch (error) {
            if (error.response && error.response.status === 401) {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Unauthorized access. Please log in again.",
                });
                AuthLogout();
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: `${error.response.data.status}`,
                });
                setActionLoading(false)
            }
        }
    }

    return {
        isLoading,
        actionLoading,
        data,
        updateStatus
    }
}