import { Grid, Paper, Stack, Typography } from "@mui/material"
import PropTypes from 'prop-types';
import { ClassCard } from "../card/ClassCard"
import { gray3Color, irisColor, whiteColor } from "../../utils/color"

export const LandingClassExplore = (Props) =>{
    return (
        <Stack justifyContent={'center'} alignItems={'center'} padding={'32px'} marginTop={'50px'} style={{
        backgroundColor: whiteColor()
        }}>
            <Typography ariant="h6" style={{ 
                textAlign: 'center', 
                margin: '0 auto', 
                fontWeight: '600', 
                fontSize:'24px',
                color: irisColor(), 
                }}>
                {Props.title}
            </Typography>
            <Grid container
                spacing={0}
                justifyContent={'center'}
                alignItems={"center"}
                flexGrow={1}
                padding={"0px"}>
                    { 
                    Props.list.map(value => {
                        return (
                        <Grid item xs={12} md={4} key={value.id}>
                            <Paper style={{ 
                                margin: '10px', 
                                boxShadow: 'none', 
                                flexDirection: 'column', 
                                alignItems: 'center', 
                                justifyContent: 'center', 
                                borderRadius: '16px' }}>
                                <ClassCard value = {value}/>
                            </Paper>
                        </Grid>
                    ) 
                }
                    )}
                </Grid>
        </Stack>
    )
}

LandingClassExplore.propTypes = {
    title: PropTypes.string.isRequired,
    // list: PropTypes.string.isRequired
    list: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number.isRequired,
      image: PropTypes.string.isRequired,
      categoryName: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired,
      // Add more PropTypes as needed for each property in your data object
    })).isRequired,
  };