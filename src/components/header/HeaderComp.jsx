import AppBar from '@mui/material/AppBar';
import { Link, useLocation } from "react-router-dom";
import { blackColor, whiteColor, yellowColor } from '../../utils/color';
import { CardMedia, Divider, Drawer, Hidden, IconButton, List, ListItem, ListItemButton, ListItemText, Toolbar, styled } from '@mui/material';
import { HeaderAfterLogin } from '../button/HeaderButtonAfterLogin';
import { HeaderBeforeLogin } from '../button/HeaderButtonBeforeLogin';
import { useContext, useEffect, useState } from 'react';
import { AuthContext } from '../../context/AuthContext';
import { ChevronRight, Menu } from '@mui/icons-material';

export const HeaderComp = ({setPathCheck}) => {
  const drawerWidth = 240;
  // const navigate = useNavigate();
  const { isAuth, AuthLogout } = useContext(AuthContext);
  const location = useLocation();
  const [open, setOpen] = useState(false);

  const [currentPath, setCurrentPath] = useState(window.location.pathname);

  const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
    justifyContent: 'flex-start',
}));

const menusBeforeLogin = [
  {
      title: "Masuk",
      page: '/login',
  },
  {
      title: "Daftar Sekarang",
      page: '/register',
  },
];

const menusAfterLogin = [
  {
      title: "Account",
      page: '/',
  },
  {
      title: "Cart",
      page: '/checkout',
  },
  {
      title: "Kelasku",
      page: '/my-class',
  },
  {
      title: "Pembelian",
      page: '/invoice',
  },
  {
      title: "LOG OUT",
      page: '/login',
  },
];

  useEffect(() => {
    setCurrentPath(window.location.pathname);
    setPathCheck(['/login', '/register', '/new-password/', '/forgot-password', "/verify/"].includes(currentPath));
  }, [window.location.pathname]);

  const isConfirmationPage = location.pathname === '/verify/' || location.pathname === '/payment-confirmed';
  
  return (
    <AppBar sx={{
      width: '100%',
      display: 'flex',
      backgroundColor: isConfirmationPage ? whiteColor() : yellowColor(),
      padding: '20px 50px',
      height: '91.51px',
      justifyContent: 'center',
      boxShadow: isConfirmationPage ? 'none' : undefined,
    }}>
      <Toolbar
      sx={{
        display:'flex',
        justifyContent:'space-between',
        padding: '0px !important',
      }}>
          <Link to='/'>
            <CardMedia
            component="img"
            alt="Image" 
            height="31.51"
            image="https://res.cloudinary.com/dayhmd2th/image/upload/v1702448518/My%20asset/aaxwempa4qlzwmh95prw.png"/>
          </Link>
        <Hidden mdDown>
          {isConfirmationPage ? (
            // Render a blank header button or any other content for the confirmation page
            <div></div>
          ) : isAuth ? <HeaderAfterLogin/> : <HeaderBeforeLogin />}
          
        </Hidden>
        <Hidden mdUp>
          <IconButton onClick={() => setOpen(true)}>
            <Menu
              sx={{ color: blackColor() }}
            />
          </IconButton>
        </Hidden>
      </Toolbar>
      <Drawer
        sx={{
          width: drawerWidth,
          flexShrink: 0,
          '& .MuiDrawer-paper': {
              width: drawerWidth,
              boxSizing: 'border-box',
          },
      }}
      variant="persistent"
      anchor="right"
      open={open}>
        <DrawerHeader>
          <IconButton onClick={() => setOpen(false)}>
            <ChevronRight/>
          </IconButton>
        </DrawerHeader>
      
      <Divider/>
      {
        isAuth
        ? <List>
            {
              menusAfterLogin.map((text) => (
                <Link
                key={text.title}
                style={{ textDecoration: 'none', color:'black'}}
                to={ text.page}
                onClick={text.title == "LOG OUT" ? AuthLogout :null}
                >
                  <ListItem key={text.title} disablePadding>
                    <ListItemButton>
                      <ListItemText primary={text.title}/>
                    </ListItemButton>
                  </ListItem>
                </Link>
              ))
            }
          </List>
          :
          <List>
            {
              menusBeforeLogin.map((text) => (
                <Link key={text.title} style={{ textDecoration: 'none', color: 'black'}}
                to={text.page}>
                  <ListItem key={text.title} disablePadding>
                    <ListItemButton>
                      <ListItemText primary={text.title}/>
                    </ListItemButton>
                  </ListItem>
                </Link>
              ))
            }
          </List>
      }
      </Drawer>
    </AppBar>
  );
  
}
