import { useEffect, useState } from 'react';
import axios from 'axios';
import { baseUrl, } from '../utils/api';

export const useDetailCourse = (productId) =>{
    const [isLoading, setIsLoading] = useState(false);
    const [detailCourse, setDetailCourse] = useState(null);
    const [otherCourseData, setOtherCourseData] = useState([]);

    const getCourse = async () => {
        try {
            setIsLoading(true);
            const responseData = await axios.get(`${baseUrl}Courses/Detail/${productId}`);
            setDetailCourse(responseData.data);

            const responseList = await axios.get(`${baseUrl}Courses?limit=10&categoryId=${responseData.data.categoryId}&courseId=${productId}`);
            setOtherCourseData(responseList.data);

        } catch (error) {
            console.error('Error fetching data:', error);
        } finally {
            setIsLoading(false);
        }
    };

    useEffect(() => {
        getCourse();
    }, [productId])

    return{
        isLoading,
        otherCourseData,
        detailCourse,
    }
}
