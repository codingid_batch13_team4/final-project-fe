import { useContext, useState } from "react"
import { AuthContext } from "../context/AuthContext"
import axios from "axios"
import { baseUrl } from "../utils/api"
import { useNavigate } from "react-router-dom"
import Swal from "sweetalert2"

export const useCreateAdminSchedule = () => {
    const { token, AuthLogout } = useContext(AuthContext)

    const navigate = useNavigate()

    const [actionLoading, setActionLoading] = useState(false)

    const createSchedule = async (time, id) => {
        setActionLoading(true)
        try {
            await axios.post(
                baseUrl + "Schedule",
                {
                    "Schedule": time,
                    "ProductId": id
                },
                {
                    headers: {
                        'accept': '*/*',
                        'Authorization': `Bearer ${token}`,
                        'Content-Type': 'multipart/form-data'
}
                }
            ).then(payload => {
                console.log(payload.data)
                Swal.fire({
                    icon: "success",
                    title: "Success Add Schedule",
                    showConfirmButton: false,
                    timer: 1500
                  });
                navigate('../admin-schedule');
            });
        } catch (error) {
            if (error.response && error.response.status === 401) {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Unauthorized access. Please log in again.",
                  });
                AuthLogout();
            } else if (error.response && error.response.status === 400) {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: `${error.response.data.status}`,
                });
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Something was Wrong",
                });
            }
        }
        setActionLoading(false)
    }

    return {
        actionLoading,
        createSchedule
    }
}