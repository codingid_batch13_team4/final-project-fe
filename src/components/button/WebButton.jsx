import { Button } from "@mui/material"
import { irisColor, whiteColor } from "../../utils/color"
import PropTypes from "prop-types";

const WebButton = (Props) => {
    return(
        <Button
        type="submit" 
        onClick={Props.action}
        variant="text"  
        style={{ 
            // display: 'flex',
            width: '176px',
            height: '55px',
            padding: '16px 24px',
            borderRadius: '6px',
            fontSize: '15px',
            fontWeight: '600',
            lineHeight: '23px',
            textAlign: 'center',            
            backgroundColor: irisColor(), 
            color: whiteColor(),
            textTransform: 'none',
        }}
        >
            {Props.title}
        </Button>
    )
}

WebButton.propTypes = {
    action: PropTypes.func,
    title: PropTypes.string.isRequired,
};

export default WebButton