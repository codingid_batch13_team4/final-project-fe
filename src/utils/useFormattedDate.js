import { useEffect, useState } from 'react';

const extractDateParts = (dateString) => {
  const [year, month, day] = dateString.split('T')[0].split('-');
  return [getDayName(day, month, year), day, month, year];
};

const getDayName = (day, month, year) => {
  const dayNames = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
  const dateObject = new Date(`${year}-${month}-${day}`);
  return dayNames[dateObject.getDay()];
};

const getMonthName = (month) => {
  const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  return months[month - 1];
};

const formatDateTime = (dateTimeString) => {
  const [dayName, day, month, year] = extractDateParts(dateTimeString);
  const dateObject = new Date(dateTimeString);
  const options = {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
  };
  return `${dayName}, ${day} ${getMonthName(month)} ${year} ${dateObject.toLocaleTimeString('en-GB', { hour: '2-digit', minute: '2-digit' })}`;
};

const useFormattedDate = (dateTimeString) => {
  const [formattedDate, setFormattedDate] = useState('');

  useEffect(() => {
    try {
      if (!dateTimeString) {
        throw new Error('Invalid date-time string');
      }

      setFormattedDate(formatDateTime(dateTimeString));
    } catch (error) {
      console.error('Error formatting date:', error);
      setFormattedDate('');
    }
  }, [dateTimeString]);

  return formattedDate;
};

export default useFormattedDate;
