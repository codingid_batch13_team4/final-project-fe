import { useNavigate } from 'react-router-dom';
import { useContext } from 'react';
import { AuthContext } from '../context/AuthContext';

export const useCustomNavigate = () => {
  const { AuthLogout } = useContext(AuthContext); 

  const navigate = useNavigate();
  const goToAdminCourse = () => navigate('admin-course');
  const goToAdminCategoryCourse = () => navigate('admin-category');
  const goToAdminPayment = () => navigate('admin-payment');
  const goToAdminUsers = () => navigate('admin-user');
  const goToAdminInvoice = () => navigate('admin-invoice');
  const goToAdminSchedule = () => navigate('admin-schedule')
  const logOut = () => AuthLogout();

  return {
    goToAdminCourse,
    goToAdminCategoryCourse,
    goToAdminPayment,
    goToAdminUsers,
    goToAdminInvoice,
    goToAdminSchedule,
    logOut,
  };
};