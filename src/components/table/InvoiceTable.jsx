import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TablePagination, Button, CircularProgress, Typography, } from '@mui/material';
import { styled } from '@mui/system';
import { tableCellClasses } from '@mui/material/TableCell';
import { useNavigate } from 'react-router-dom';
import { blackColor, gray2Color, yellowColor } from '../../utils/color';
import moment from 'moment';
import 'moment/locale/id';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: yellowColor(), // Head yellow color
    color: gray2Color(), // Text color for head cells
    fontSize: 16,
    fontWeight: 600,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme, evenrow }) => ({
  backgroundColor: evenrow === 'true' ? theme.palette.common.white : theme.palette.grey[200],
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

const Row = ({ row, index }) => {
  const navigate = useNavigate();

  const handleRincianClick = (InvoiceNo) => {
    navigate(`/detail-invoice/${InvoiceNo}`);
  };

  return (
    <>
      <StyledTableRow key={index} evenrow={(index % 2 === 0).toString()} height={'81px'}>
        <TableCell align="center">{index + 1}</TableCell>
        <TableCell align="center">{row.noInvoice}</TableCell>
        <TableCell align="center">{moment(row.createdAt).locale('id').format('dddd, DD MMMM YYYY [at] HH:mm')}</TableCell>
        <TableCell align="center">{row.totalOrders}</TableCell>
        <TableCell align="center"> IDR {row.totalPrice.toLocaleString()}</TableCell>
        <TableCell align="center">
          <Button
            variant="contained"
            onClick={() => handleRincianClick(row.noInvoice)}
          >
            Rincian
          </Button>
        </TableCell>
      </StyledTableRow>
    </>
  );
}

export const InvoiceTable = ({ ListInvoice, loading }) => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);

  const handleChangePage = (_, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return (
    <TableContainer component={Paper}>
      <Table aria-label="collapsible table">
        <TableHead>
          <TableRow>
            <StyledTableCell align="center">No</StyledTableCell>
            <StyledTableCell align="center">No. Invoice</StyledTableCell>
            <StyledTableCell align="center">Tanggal Beli</StyledTableCell>
            <StyledTableCell align="center">Jumlah Kursus</StyledTableCell>
            <StyledTableCell align="center">Total Harga</StyledTableCell>
            <StyledTableCell align="center">Action</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {loading ? (
            <StyledTableRow>
              <StyledTableCell colSpan={6} align="center">
                <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: '20vh' }}>
                  <CircularProgress />
                </div>
              </StyledTableCell>
            </StyledTableRow>
          ) : (
            ListInvoice && ListInvoice.length > 0 ? (
              ListInvoice.slice(page * rowsPerPage, (page + 1) * rowsPerPage).map((row, index) => (
                <Row key={row.id} row={row} index={page * rowsPerPage + index} />
              ))
            ) : (
              <StyledTableRow>
                <StyledTableCell colSpan={6} align="center">
                  <Typography style={{ fontWeight: '400', color: blackColor(), textAlign: 'center', typography: { lg: "h5", md: "h5", sm: "body1", xs: "body1" } }}>
                    No Item Available.
                  </Typography>
                </StyledTableCell>
              </StyledTableRow>
            )
          )}
        </TableBody>
      </Table>
      <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={ListInvoice.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </TableContainer>
  );
}
