import { useContext, useEffect, useState } from 'react';
import axios from 'axios';
import { baseUrl, } from '../utils/api';
import { AuthContext } from '../context/AuthContext';

export const useInvoice = () => {

    const { token, Authlogout } = useContext(AuthContext)

    const [isLoading, setLoading] = useState(false)
    const [isListLoading, setIsListLoading] = useState(false);
    const [listInvoice, setListInvoice] = useState([]);
    const [detailInvoice, setDetailInvoice] = useState([]);

    const getDataInvoice = async () => {
        try {
            setIsListLoading(true)
            const response = await axios.get(`${baseUrl}Invoice`,
                { headers: { 'Authorization': `Bearer ${token}` } });
            setListInvoice(response.data);
        } catch (error) {
            if (error.response && error.response.status === 401) {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Unauthorized access. Please log in again.",
                  });
                AuthLogout();
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Something was Wrong",
                });
            }
        } finally {
            setIsListLoading(false)
        }
    }

    const getDetailInvoice = async (invoiceNumber) => {
        try {
            setIsListLoading(true);
            const response = await axios.get(`${baseUrl}Invoice/Detail?number=${invoiceNumber}`, {
                headers: { 'Authorization': `Bearer ${token}` }
            });
            setDetailInvoice(response.data);
        } catch (error) {
            if (error.response && error.response.status === 401) {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Unauthorized access. Please log in again.",
                  });
                AuthLogout();
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Something was Wrong",
                });
            }
        } finally {
            setIsListLoading(false)
        }
    }

    useEffect(() => {
        setLoading(true);
        getDataInvoice();
        setLoading(false);
    }, [])

    return {
        isListLoading,
        listInvoice,
        detailInvoice,
        getDetailInvoice,
        isLoading,
    }
}
