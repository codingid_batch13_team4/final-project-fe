import { useContext, useEffect, useState } from "react"
import { AuthContext } from "../context/AuthContext"
import axios from "axios"
import { baseUrl } from "../utils/api"

export const useEditPaymentAdmin = (id) => {
    const [isLoadingPayment, setLoading] = useState(true)
    const [data, setData] = useState(null)
    const { token, AuthLogout } = useContext(AuthContext)

    const getDataPaymentId = async () => {
        try {
            await axios.get(
                baseUrl + `Payment/${id}`,
                { headers: { 'Authorization': `Bearer ${token}` } }
            ).then(payload => {
                setData(payload.data)
                setLoading(false)
            })
        } catch (error) {
            if (error.response && error.response.status === 401) {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Unauthorized access. Please log in again.",
                  });
                AuthLogout();
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Something was Wrong",
                });
            }
        }
    }
    useEffect(() => {
        getDataPaymentId()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    
    return {
        isLoadingPayment,
        data,
        baseUrl
    }
}