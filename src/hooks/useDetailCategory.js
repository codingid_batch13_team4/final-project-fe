import { useEffect, useState } from 'react';
import axios from 'axios';
import { baseUrl, } from '../utils/api';

export const useDetailCategory = (id) =>{
    const [isCategoryLoading, setIsCategoryLoading] = useState(false);
    const [categoryData, setCategoryData] = useState(null);

    const getCategory = async () => {
        try {
            setIsCategoryLoading(true);
            const response = await axios.get(`${baseUrl}Category/${id}`);
            setCategoryData(response.data);
        } catch (e) {
            console.error('Error fetching data:', e.message);
        } finally {
            setIsCategoryLoading(false);
        }
    };

    useEffect(() => {
        getCategory();
    }, [id])

    return{
        isCategoryLoading,
        categoryData,
    }
}
