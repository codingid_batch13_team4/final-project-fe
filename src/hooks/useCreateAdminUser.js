import { useContext, useState } from "react"
import { AuthContext } from "../context/AuthContext"
import axios from "axios"
import { baseUrl } from "../utils/api"
import Swal from "sweetalert2"
import { useNavigate } from "react-router-dom"

export const useCreateAdminUser = () => {
    const { token, AuthLogout } = useContext(AuthContext)

    const navigate = useNavigate();

    const [isLoading, setLoading] = useState(false)

    const registerAdmin = async (name, email, password, role) => {
        setLoading(true)

        try {
            await axios.post(
                baseUrl + "Auth/RegisterAdmin",
                {
                    "name": name,
                    "email": email,
                    "password": password,
                    "role": role,
                    "isActive": true
                }
            ).then(() => {
                Swal.fire({
                    icon: "success",
                    title: "User berhasil ditambahkan!",
                    showConfirmButton: false,
                    timer: 1500
                  });
                navigate('../admin-user');

            });

        } catch (error) {
            console.log("Error nya adalah " + error)
            Swal.fire({
                icon: "error",
                title: "Oops...",
                text: "Periksa kembali apakah email sudah di pakai atau belum",
              });

        }
        setLoading(false)
    }

    const updateDataUser = async (id, name, email, role, isActive) => {
        setLoading(true)
        try {
            await axios.patch(baseUrl + "Auth/" + id,
                {
                    "name": name,
                    "email": email,
                    "password": null,
                    "role": role,
                    "isActive": isActive
                },
                {
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                }).then(() => {
                    Swal.fire({
                        icon: "success",
                        title: "Edit User Berhasil!",
                        showConfirmButton: false,
                        timer: 1500
                      });
                    navigate('../admin-user');
                });

        } catch (error) {

            if (error.response && error.response.status === 401) {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Unauthorized access. Please log in again!",
                  });
                AuthLogout();
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Something wes Wrong",
                  });

            }

        }
        setLoading(false)

    }

    return {
        registerAdmin,
        updateDataUser,
        isLoading
    }
}