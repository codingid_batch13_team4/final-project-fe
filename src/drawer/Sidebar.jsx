import { 
  AccountBalance, 
  AccountBox, 
  Apple, 
  CalendarMonth, 
  Category, 
  ChevronLeft, 
  LibraryMusic, 
  Logout, 
  Receipt } from "@mui/icons-material"
import { 
    Box, 
    CardMedia, 
    Container, 
    CssBaseline, 
    Divider, 
    IconButton, 
    List, 
    ListItemButton, 
    ListItemIcon, 
    ListItemText, 
    Toolbar, 
    Typography
} from "@mui/material"
import { useCustomNavigate } from "../hooks/useAdminNavigation";
import { useState } from "react";
import { blackColor, whiteColor, yellowColor } from "../utils/color";
// import Menu from "@mui/icons-material/Menu";
import * as React from "react";
import { AppBar, Drawer } from "./StyledSidebar";
// import HeaderAdmin from "../components/header/HeaderAdmin";


export const Sidebar = ({ children })=> {
    const [open, setOpen] = useState(true);
    const toggleDrawer = () => {
        setOpen(!open);
    };

    const {
        goToAdminCourse,
        goToAdminCategoryCourse,
        goToAdminPayment,
        goToAdminUsers,
        goToAdminInvoice,
        goToAdminSchedule,
        logOut
    } = useCustomNavigate();

    const listMenus = [
      {
          icon: <Category />,
          title: "Category",
          navigate: goToAdminCategoryCourse
      },
      {
          icon: <LibraryMusic />,
          title: "Course",
          navigate: goToAdminCourse 
      },
      {
          icon: <AccountBox />,
          title: "User",
          navigate: goToAdminUsers
      },
      {
          icon: <AccountBalance />,
          title: "Payment",
          navigate: goToAdminPayment
      },
      {
          icon: <Receipt />,
          title: "Invoice",
          navigate: goToAdminInvoice
      },
      {
          icon: <CalendarMonth />,
          title: "Schedule",
          navigate: goToAdminSchedule
      },
      {
        icon: <Logout />,
        title: "Log Out",
        navigate: logOut
    },
  ]

    return (
        
    <Box sx={{ display: "flex" }}>
    <CssBaseline />
    {/* <HeaderAdmin/> */}
    <Drawer
      variant="permanent" open={open}
    >
      <Toolbar 
      sx={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        gap: '10px',
      }}>
        <CardMedia
            component="img"
            alt="Image" 
            height="30"
            image="https://res.cloudinary.com/dayhmd2th/image/upload/v1702448518/My%20asset/aaxwempa4qlzwmh95prw.png"/>
        <IconButton onClick={toggleDrawer}>
          <ChevronLeft/>
        </IconButton>
      </Toolbar>
      <Divider />
      <List component="nav">
        {
          listMenus.map((value, index) => {
            return (
              <ListItemButton onClick={value.navigate} key={index}>
                <ListItemIcon>
                  {value.icon}
                </ListItemIcon>
              <ListItemText primary={value.title} />
              </ListItemButton>
            )
          })
        }
    </List>
    </Drawer>
    <Box 
    component="main"
    sx={{
        backgroundColor: whiteColor,
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
    }}>
      <Toolbar />
      <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
        {children}
      </Container>
    </Box>
  </Box>

    )
}