import { useContext, useEffect, useState } from "react"
import { AuthContext } from "../context/AuthContext"
import axios from "axios"
import { baseUrl } from "../utils/api"

export const useEditCategory = (id) => {
    const [isLoadingEdit, setLoading] = useState(true)
    const [data, setData] = useState(null)
    const { token, AuthLogout } = useContext(AuthContext)

    const getDataCategoryById = async () => {
        try {
            await axios.get(
                baseUrl + `Category/${id}`,
                {
                    headers: { 'Authorization': `Bearer ${token}`}
                }
            ).then(paylaod => {
                setData(paylaod.data)
                setLoading(false)
            })
        } catch (error) {
            if (error.response && error.response.status === 401) {
                AuthLogout();
            } else {
                console.error(error);
                setLoading(false)
            }
        }
    }

    useEffect(() => {
        getDataCategoryById()
    },[])

    return {
        isLoadingEdit,
        data,
        baseUrl
    }
}
