import { useContext, useEffect, useState } from "react"
import { AuthContext } from "../context/AuthContext"
import axios from "axios"
import { baseUrl } from "../utils/api"
import Swal from "sweetalert2"
import { useNavigate } from "react-router-dom"

export const useCreateAdminCourse = () => {
    const { token, Authlogout } = useContext(AuthContext)

    const [isLoading, setLoading] = useState(true)
    const [actionLoading, setActionLoading] = useState(false)
    const [category, setCategory] = useState(null)
    const navigate = useNavigate();
    const getCategory = async () => {
        try {

            await axios.get(baseUrl + "Category?status=true").then(payload => {
                setCategory(payload.data)
            })

        } catch (error) {

            if (error.response && error.response.status === 401) {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Unauthorized access. Please log in again.",
                  });
                AuthLogout();
            } else if (error.response && error.response.status === 400) {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: `${error.response.data.status}`,
                });
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Something was Wrong",
                });
            }

        }

        setLoading(false)
    }

    useEffect(() => {
        getCategory()
    }, [])

    const createCourse = async (title, desc, idCategory, price, image, isActive) => {
        setActionLoading(true)
        const formData = new FormData();
        formData.append('TitleCourse', title);
        formData.append('DescriptionCourse', desc);
        formData.append('CategoryId', parseInt(idCategory));
        formData.append('Price', parseInt(price));
        formData.append('Image', image);
        formData.append('IsActive', isActive);

        try {
            await axios.post(baseUrl + "Courses", formData, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Authorization': `Bearer ${token}`
                },
            }).then(() => {
                Swal.fire({
                    icon: "success",
                    title: "Success Add Course",
                    text: "Please Add Schedule",
                    showConfirmButton: false,
                    timer: 1500
                  });
                navigate('../add-schedule');
            });
        } catch (error) {
            if (error.response && error.response.status === 401) {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Unauthorized access. Please log in again.",
                });
                Authlogout();
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Something was Wrong",
                });
            }
        }        
        setActionLoading(false)
    }

    const updateCourse = async (title, desc, idCategory, price, image, isActive, id) => {
        setActionLoading(true)
        const formData = new FormData();
        formData.append('TitleCourse', title);
        formData.append('DescriptionCourse', desc);
        formData.append('CategoryId', idCategory);
        formData.append('Price', parseInt(price));
        formData.append('Image', image);        
        formData.append('IsActive', isActive);

        try {
            await axios.put(baseUrl + "Courses/Update/" + id, formData, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Authorization': `Bearer ${token}`
                },
            }).then(() => {
                Swal.fire({
                    icon: "success",
                    title: "Success Edit Course",
                    showConfirmButton: false,
                    timer: 1500
                  });
                navigate('../admin-course');
            });

        } catch (error) {

            if (error.response && error.response.status === 401) {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Unauthorized access. Please log in again.",
                  });
                AuthLogout();
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Something was Wrong",
                });

            }

        }
        setActionLoading(false)
    }

    return {
        isLoading,
        category,
        actionLoading,
        createCourse,
        updateCourse
    }
}