import { Box, Typography } from "@mui/material";
import { whiteColor } from "../../utils/color";

export const ListClassMenuThumbnail = ({ imageURL, categoryType }) => {
    return (
        <Box
          sx={{
            backgroundImage: `url(${imageURL})`,
            backgroundPosition: 'center',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            marginTop: '91.5px',
            color: 'white',
            height: '334px',
            position: 'relative',
            filter: 'brightness(0.95)',
          }}
        >
          <Typography
            variant="h1"
            color="textPrimary"
            sx={{
              paddingRight: {xs:'19.5px', md:'8px'},
              fontWeight: '500',
              color: whiteColor(),
              position: 'absolute',
              bottom: 0,
              right: 0,
              WebkitTextStroke: '2px black',
              textShadow: '10px 10px 10px rgba(0, 0, 0, 0.5)',
              textTransform :'upperCase',
              fontSize: {xs:'45px', md:'76px'},
              textAlign:'right',
          }}>
            Learn the {categoryType}'s
          </Typography>
        </Box>
      );
}