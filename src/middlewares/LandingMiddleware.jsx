import { useContext, useEffect } from "react"
import { AuthContext } from "../context/AuthContext"
import { useNavigate } from "react-router-dom"

export const LandingMiddleware = ({children}) => {
    const { isAdmin } = useContext(AuthContext)
    const navigate = useNavigate()

    useEffect(() => {
        if (isAdmin) {
            return navigate('/admin')
        }
    },[isAdmin,  navigate])

    return !isAdmin ? children : null;
}