import { Box, Typography, Stack, CircularProgress } from '@mui/material';
import { Link } from 'react-router-dom';
import { blackColor, gray2Color, gray3Color, irisColor } from '../utils/color';
import { InvoiceTable } from '../components/table/InvoiceTable';
import { FooterPage } from '../components/footer/FooterPage';
import { useInvoice } from '../hooks/useListInvoice';

export const InvoicePage = () => {
  const { isListLoading, listInvoice, } = useInvoice();
  return (
    <Box >
      <Stack minHeight={'100vh'} width={'100%'} justifyContent={'space-between'}>
        <Stack marginTop={'132px'} marginBottom={'20px'} justifyContent={'start'} alignItems={'start'} padding={'0px 5.2%'} flex={1} gap={'32px'}>
          <Stack flexDirection={'row'}>
            <Link to={'/'} style={{ textDecoration: 'none' }}>
              <Typography variant="body1" fontWeight={'600'} color={gray3Color} marginRight={'5px'}>
                {"Home >"}
              </Typography>
            </Link>
            <Typography variant="body1" fontWeight={'600'} color={irisColor} >
              Invoice
            </Typography>
          </Stack>
          <Box width={'100%'} sx={{ display: 'flex', flexDirection: 'column', gap: '24px' }}>
            <Typography variant="body1" fontWeight={'700'} color={gray2Color} >
              Menu Invoice
            </Typography>
            <InvoiceTable loading={isListLoading} ListInvoice={listInvoice} />
          </Box>
        </Stack>
        <FooterPage />
      </Stack>
    </Box>
  );
};
