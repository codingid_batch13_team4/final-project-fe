import { Button } from "@mui/material";
import { irisColor, whiteColor } from "../../utils/color";
import PropTypes from "prop-types";

const FlatButton = (Props) => {
    return(
        <Button
        type="submit" 
        onClick={Props.action}
        variant="text"  
        style={{ 
            // display: 'flex',
            width: '140px',
            height: '43px',
            padding: '10px',
            borderRadius: '8px',
            fontSize: '15px',
            fontWeight: '500',
            lineHeight: '23px',
            textAlign: 'center',            
            backgroundColor: irisColor(), 
            color: whiteColor(),
            textTransform: 'none',
        }}
        >
            {Props.title}
        </Button>
    )
}

FlatButton.propTypes = {
    action: PropTypes.func,
    title: PropTypes.string.isRequired,
};

export default FlatButton