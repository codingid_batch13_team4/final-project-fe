import { 
    Box, 
    Button, 
    FormControl, 
    InputLabel, 
    MenuItem, 
    Select, 
    TextField, 
    Typography } from "@mui/material";
import { useAdminCourse } from "../../hooks/useAdminCourse";
import { useCreateAdminSchedule } from "../../hooks/useCreateAdminSchedule";
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

export const AdminAddSchedule = () => {
    const navigate = useNavigate();
    const { data, isLoading }= useAdminCourse()
    const { actionLoading, createSchedule } = useCreateAdminSchedule()

    const [schedule, setSchedule] = useState('');
    const [productId, setProductId] = useState('');
    const [courses, setCourses] = useState([]);

    useEffect(() => {
        if (!isLoading) {
            setCourses(data)
        }
    }, [isLoading, data])

    const shouldDisableTime= (value, view) => {
        view === 'hours' && value.hour() <= 10 && value.hour() >= 16
    }

    const isInCurrentYear = (date) => date.get('year') === dayjs().get('year');

    const handleSubmit = (e) => {
        e.preventDefault();

        if (!actionLoading) {
            createSchedule(schedule, productId)
        }

    };
    if (isLoading) {
        return <h1>Loading ...</h1>
    }
    return (
        <LocalizationProvider dateAdapter={AdapterDayjs}>
        <Box p={3}>
            <form autoComplete="off" onSubmit={handleSubmit}>
                <Typography variant='h5' marginBottom={'60px'}>Create Schedule</Typography>
                <DateTimePicker
                disablePast
                maxDate={isInCurrentYear}
                shouldDisableTime={shouldDisableTime}
                ampm={false}
                sx={{mb: 2, width:'100%'}}
                    label="Schedule"
                    value={schedule}
                    onChange={(newValue) => setSchedule(newValue)}
                />
                <FormControl fullWidth variant="outlined" sx={{ mb: 2 }}>
                    <InputLabel id="courseFK-label">Course</InputLabel>
                    <Select
                        required
                        labelId="courseFK-label"
                        label="Course"
                        value={productId}
                        onChange={(e) => setProductId(e.target.value)}
                    >
                        {courses.map((course) => (
                            <MenuItem key={course.id} value={course.id}>
                                {course.name}
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
                <Button
                    variant="contained"
                    color="primary"
                    type="submit"
                    style={{ marginTop: '16px' }}
                >
                    {actionLoading ? "Loading" : 'Save'}
                </Button>
            </form>
        </Box>
    </LocalizationProvider>
    )
}