import { useContext, useEffect } from "react"
import { AuthContext } from "../context/AuthContext"
import { useNavigate } from "react-router-dom"

export const GuestMiddleware = ({ children }) => {
    const { isAuth, isAdmin } = useContext(AuthContext)
    const navigate = useNavigate()

    useEffect(() => {
        if (isAdmin) {
            return navigate('/admin')
        }
        else if (isAuth) {
            return navigate('/')
        }
    },[isAdmin, isAuth, navigate])

    return !isAuth || !isAdmin ? children : null;
}
