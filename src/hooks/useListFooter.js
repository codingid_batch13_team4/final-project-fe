import { useEffect, useState } from 'react';
import axios from 'axios';
import { baseUrl, } from '../utils/api';

export const useListFooter = () => {
    // const [isAllCategoryLoading, setIsAllCategoryLoading] = useState(false);
    const [listCategory, setListCategory] = useState(null);

    const getAllCategory = async () => {
        try {
            // setIsAllCategoryLoading(true);
            const response = await axios.get(`${baseUrl}Category?status=true`);
            const shuffledList = response.data.sort(() => Math.random() - 0.5);
            const selectedItems = shuffledList.slice(0, 6);
            const splitProducts = [selectedItems.slice(0, 3), selectedItems.slice(3, 6)];
            setListCategory(splitProducts);
        } catch (error) {
            console.error('Error fetching data:', error);
        } 
        // finally {
        //     setIsAllCategoryLoading(false);
        // }
    };

    useEffect(() => {
        getAllCategory();
    }, [window.location.pathname])

    return {
        listCategory,
    }

}
