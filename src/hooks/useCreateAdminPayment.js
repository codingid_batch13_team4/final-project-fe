import { useContext, useState } from "react"
import { AuthContext } from "../context/AuthContext"
import { baseUrl } from "../utils/api"
import axios from "axios"
import { useNavigate } from "react-router-dom"
import Swal from "sweetalert2"

export const useCreateAdminPayment = () => {
    const { token, AuthLogout } = useContext(AuthContext)

    const [isLoading, setLoading] = useState(false)
    
    const navigate = useNavigate();

    const createPayment = async (title, image, isActive) => {
        setLoading(true)

        const formData = new FormData();
        formData.append('Name', title);
        formData.append('Image', image);
        formData.append('Status', isActive);

        try {
            await axios.post(baseUrl + "Payment", formData, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Authorization': `Bearer ${token}`
                },
            }).then(() => {
                Swal.fire({
                    icon: "success",
                    title: "Success Add Payment",
                    showConfirmButton: false,
                    timer: 1500
                  });
                navigate('../admin-payment');
            });

        } catch (error) {

            if (error.response && error.response.status === 401) {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Unauthorized access. Please log in again.",
                  });
                AuthLogout();
            } else if (error.response && error.response.status === 400) {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: `${error.response.data.status}`,
                });
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Something was Wrong",
                });
            }

        }
        setLoading(false)
    }

    const updatePayment = async (title, image, isActive, id) => {
        setLoading(true)

        const formData = new FormData();
        formData.append('Name', title);
        formData.append('Image', image);
        formData.append('status', isActive);

        try {
            await axios.put(baseUrl + "Payment/" + id, formData, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Authorization': `Bearer ${token}`
                },
            }).then(() => {
                Swal.fire({
                    icon: "success",
                    title: "Success Edit Payment",
                    showConfirmButton: false,
                    timer: 1500
                  });
                navigate('../admin-payment');
            });

        } catch (error) {

            if (error.response && error.response.status === 401) {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Unauthorized access. Please log in again!",
                  });
                AuthLogout();
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Something wes Wrong",
                  });

            }

        }
        setLoading(false)
    }

    return {
        createPayment,
        updatePayment,
        isLoading
    }
}