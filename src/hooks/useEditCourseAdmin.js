import { useContext, useEffect, useState } from "react"
import { AuthContext } from "../context/AuthContext"
import axios from "axios"
import { baseUrl } from "../utils/api"

export const useEditCourseAdmin = (id) => {
    const [isLoadingProduct, setLoading] = useState(true)
    const [data, setData] = useState(null)
    const { token, Authlogout } = useContext(AuthContext)

    const getDataCourseById = async () => {
        try {
            await axios.get(
                baseUrl + `Courses/Detail/${id}`,
                { headers: { 'Authorization': `Bearer ${token}` } }
            ).then(payload => {
                console.log(payload.data)
                setData(payload.data)
                setLoading(false)
            })
        } catch (error) {
            if (error.response && error.response.status === 401) {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Unauthorized access. Please log in again.",
                  });
                AuthLogout();
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Something was Wrong",
                });
            }
        }
    }

    useEffect(() => {
        getDataCourseById()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    return {
        isLoadingProduct,
        data,
        baseUrl
    }
}