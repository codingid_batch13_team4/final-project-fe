import { createContext, useState } from "react";

export const CartContext = createContext(null);

export default function CartProvider({ children }) {
  const [cartAmount, setCart] = useState([]);

  const CountCart = (Cartdata) => {
    setCart(Cartdata.length);
  };

  return (
    <CartContext.Provider
      value={{
        cartAmount,
        CountCart,
      }}
    >
      {children}
    </CartContext.Provider>
  );
}
