import axios from "axios";
import Cookies from "js-cookie";
import { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { ROLE, TOKEN } from "../utils/cookieVariable";
import { baseLink, baseUrl } from "../utils/api";
import { AuthContext } from "../context/AuthContext";
import Swal from "sweetalert2";

export const useAuth = () => {

    const navigate = useNavigate();

    const [isLoading, setLoading] = useState(false);

    const { AuthLogin } = useContext(AuthContext);

    const login = async (email, password, pathCheck) => {
        setLoading(true)
        console.log("we are going to landing page ", pathCheck);

        try {
            await axios.post(
                baseUrl + "Auth/Login",
                {
                    "email": email,
                    "password": password
                }
            ).then(placement => {
                Cookies.set(ROLE, placement.data.role, { 'expires': 1 })
                Cookies.set(TOKEN, placement.data.tokenUser, { 'expires': 1 })
                if (placement.data.role == 'ADMIN') {
                    AuthLogin('ADMIN')
                    navigate("/admin")
                }
                else if (pathCheck) {
                    AuthLogin('USER')
                    navigate('/');
                } else {
                    AuthLogin('USER')
                    navigate(-1);
                }
            })

        } catch (error) {
            if (error.response && error.response.status === 400) {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: `${error.response.data.status}`,
                    text: `Silahkan periksa email untuk aktivasi`,
                });

            } else if (error.response && error.response.status === 404) {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Akun tidak ditemukan",
                    text: `${error.response.data.status}`,
                });

            } else {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Terjadi Kendala. Segera Hubungi CS"
                });
            }

        }
        setLoading(false)
    }

    const register = async (name, email, password) => {
        setLoading(true)
        let link = `${baseLink}verify/?verificationToken=`

        try {
            await axios.post(
                baseUrl + `Auth/Register?link=${link}`,
                {
                    "name": name,
                    "email": email,
                    "password": password,
                    "role": "USER",
                    "status": false
                }
            ).then(() => {
                Swal.fire({
                    title: "Registrasi berhasil.",
                    text: "Silahkan periksa Email anda",
                    icon: "success"
                });
                navigate("/login");
            });
        } catch (error) {
            if (error.response && error.response.status === 400) {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: `${error.response.data.status}`,
                });
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Gagal Registrasi. Hubungi CS",
                });
            }
        }
        setLoading(false)
    }

    const ConfirmPassword = async (verifToken) => {
        setLoading(true)
        try {
            await axios.put(baseUrl + "Auth?verificationToken=" + verifToken).then(() => {
                setLoading(false)
            })
        } catch (error) {
            console.log(error)
        }
    }

    const ForgotPassword = async (email) => {
        setLoading(true)
        let link = `${baseLink}new-password/?email=`
        try {
            await axios.post(
                baseUrl + "Auth/ForgotPassword",
                {
                    "email": email,
                    "link": link
                }
            ).then(() => {
                Swal.fire({
                    title: "Permintaan Anda berhasil.",
                    text: "Silahkan periksa Email anda",
                    icon: "success"
                });
            })
        } catch (error) {
            if (error.response && error.response.status === 404) {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: `${error.response.data.status}`,
                    text: "Check your email or register your account",
                });
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Gagal Registrasi. Hubungi CS",
                });
            }
        }
        setLoading(false)
    }

    const CreateNewPassword = async (email, password, token) => {
        setLoading(true)
        try {
            await axios.post(
                baseUrl + "Auth/ResetPassword",
                {
                    "email": email,
                    "newPassword": password,
                    "token": token
                }
            ).then(() => {
                Swal.fire({
                    title: "Berhasil reset Password",
                    text: "Anda sudah bisa login",
                    icon: "success"
                });
                navigate("/login");
            });
        } catch (error) {
            if (error.response && error.response.status === 404) {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Anda Belum Mempunyai Akun",
                    text: `Atau Akun anda tidak aktif, silahkan periksa email untuk aktivasi`,
                });

            } else if (error.response && error.response.status === 400) {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Reset Password Gagal, silahkan hubungi CS",
                });

            } else {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Terjadi Kendala. Segera Hubungi CS"
                });
            }
        }
        setLoading(false)
    }

    return {
        isLoading,
        login,
        register,
        ConfirmPassword,
        ForgotPassword,
        CreateNewPassword
    }
}