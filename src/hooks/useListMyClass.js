import { useContext, useEffect, useState } from 'react';
import axios from 'axios';
import { baseUrl, } from '../utils/api';
import { AuthContext } from '../context/AuthContext';

export const useListMyClass = () =>{

    const { token, Authlogout } = useContext(AuthContext)

    const [isListCourseLoading, setIsListCourseLoading] = useState(false);
    const [ListCourseData, setListCourseData] = useState([]);

    const getDataClass = async () => {
        try {
            setIsListCourseLoading(true)
            const response = await axios.get(`${baseUrl}Invoice/MyClass`,
            { headers: { 'Authorization': `Bearer ${token}` } });
            setListCourseData(response.data)
        } catch (error) {
            if (error.response && error.response.status === 401) {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Unauthorized access. Please log in again.",
                  });
                AuthLogout();
            } else {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Something was Wrong",
                });
            }
        } finally {
            setIsListCourseLoading(false)
        }
    }

    useEffect(() => {
        getDataClass();
    }, [])
    return{
        isListCourseLoading,
        ListCourseData,
    }
}
