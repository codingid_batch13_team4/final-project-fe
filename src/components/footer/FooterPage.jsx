import { Box, CircularProgress, Container, Grid } from "@mui/material";
import { yellowColor } from "../../utils/color";
import { TitlewDesc } from "../contains/TitlewDesc";
import { FooterContact } from "./FooterContact";
import { FooterProductList } from "./FooterProductList";
import { useListFooter } from "../../hooks/useListFooter";

export const FooterPage = () => {
    const { listCategory } = useListFooter();
    if (!listCategory){
        return (
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: '100vh' }}>
                <CircularProgress />
            </div>
        );
    }

        return (
            <Box
            padding= { {lg: "0px 5px", md: "0px 5px", sm: "0px 8px", xs: "0px 8px"} }
                sx={{
                    backgroundColor: yellowColor(),
                    marginTop: 'auto',
                    position:'relative',
                    width:'100%',
                    bottom: 0,
                    paddingBottom: '50px',
                    minHeight: '220px',
                    display: 'flex',
                }}
            >
                <Container >
                    <Grid container spacing={0} marginTop={'24px'} marginLeft={'0px'}>
                        <Grid item sm={12} md={5} sx={{ display: 'flex', justifyContent:'center', width :'100%', padding: "0px !important" }}>
                            <TitlewDesc
                                title="Tentang"
                                titleVariant="body1"
                                titleFontWeight='500'
                                desc="Arts funding is often the first thing to get cut, so artists often have a hard time finding their way. Our goal is to help artists tell their stories to a broader audience and connect them to a community that appreciates the arts."
                                descVariant='subtitle2'
                                descFontWeight='400'
                                textGap='8px'
                                textAligning='justify'
                            />
                        </Grid>
                        <Grid item sm={12} md={3} sx={{ display: 'flex', justifyContent:'center', width :'100%', padding: "0px !important" }}>
                            <FooterProductList list={listCategory} />
                        </Grid>
                        <Grid item sm={12} md={4} sx={{ display: 'flex', justifyContent:'center', width :'100%', padding: "0px !important" }}>
                            <Box sx={{ height:'100%', display: 'flex', flexDirection: 'column', gap: '16px' }}>
                                <TitlewDesc
                                    title="Alamat"
                                    titleVariant="body1"
                                    titleFontWeight='500'
                                    desc="Jalan Merdeka No. 50, Jakarta Selatan"
                                    descVariant="subtitle2"
                                    descFontWeight='400'
                                    textGap='8px'
                                    textAligning='justify'
                                />
                                <FooterContact />
                            </Box>
                        </Grid>
                    </Grid>
                </Container>
            </Box>
        );
}