import { Box, Container, Grid, Paper, Typography } from "@mui/material"
import PropTypes from 'prop-types';
import { Link } from "react-router-dom"
import { TransparentBlue, blackColor } from "../../utils/color"
import { imgUrl } from "../../utils/api";

export const LandingClassCategory = (Props) => {
return (
    <Box maxWidth="100%" style={{ marginTop: '100px', backgroundColor: TransparentBlue() }}>
    <Container maxWidth="md" style={{ paddingTop: '50px', paddingBottom: '60px' }}>
      <Typography variant="h6" style={{ textAlign: 'center', margin: '0 auto', fontWeight: '600', color: '#5D5FEF', marginBottom: '20px' }}>
      Pilih kelas impian kamu
      </Typography>

      <br />
      <Grid container spacing={4}>
        {Props.data.map(value =>(
          <Grid item xs={6} lg={3} key={value.name}>
            <Link to={`/list-menu/${value.id}`} style={{ textDecoration: 'none' }}>
              <Paper style={{ backgroundColor: 'transparent', boxShadow: 'none', display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'start', borderRadius: 0 }}>
                <div style={{ width: '70%', height: 0, paddingBottom: '47%', position: 'relative', borderRadius: '16px', overflow: 'hidden' }}>
                  <img src={`${imgUrl}${value.image}`} alt={value.name} style={{ position: 'absolute', width: '100%', height: '100%', objectFit: 'cover', borderRadius: 'inherit' }} />
                </div>

                <br />
                <Typography variant="h6" component="p" color="textPrimary" style={{ color: blackColor(), fontWeight: '400' }}>
                {value.name}
                </Typography>
              </Paper>
            </Link>
          </Grid>
        ))}
      </Grid>
    </Container>
  </Box>
)
};

LandingClassCategory.propTypes = {
  data: PropTypes.array.isRequired
  // data: PropTypes.string.isRequired
  // data: PropTypes.arrayOf(PropTypes.shape({
  //   id: PropTypes.number.isRequired,
  //   name: PropTypes.string.isRequired,
  //   image: PropTypes.string.isRequired,
  //   // Add more PropTypes as needed for each property in your data object
  // })).isRequired,
};