import { useEffect, useState } from 'react';
import axios from 'axios';
import { baseUrl, } from '../utils/api';

export const useListLanding = () =>{
    const [isListCourseLoading, setIsListCourseLoading] = useState(false);
    const [ListCourseData, setListCourseData] = useState([]);
    const [isAllCategoryLoading, setIsAllCategoryLoading] = useState(false);
    const [allCategoryData, setAllCategoryData] = useState(null);

    const getAllCategory = async () => {
        try {
            setIsAllCategoryLoading(true);
            const response = await axios.get(`${baseUrl}Category?status=true`);
            setAllCategoryData(response.data);
        } catch (error) {
            console.error('Error fetching data:', error);
        } finally {
            setIsAllCategoryLoading(false);
        }
    };

    const getDataClass = async () => {
        try {
            setIsListCourseLoading(true)
            const response = await axios.get(`${baseUrl}Courses?limit=6`);
            setListCourseData(response.data)
        } catch (error) {
            console.error('Error fetching data:', error);
        } finally {
            setIsListCourseLoading(false)
        }
    }

    useEffect(() => {
        getDataClass();
        getAllCategory();
    }, [])
    return{
        isListCourseLoading,
        ListCourseData,
        isAllCategoryLoading,
        allCategoryData,
    }
}
