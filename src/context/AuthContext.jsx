import Cookies from "js-cookie";
import { createContext, useState } from "react";
import { ROLE, TOKEN } from "../utils/cookieVariable";



// const isLoggedIn = () => {
//     const token = Cookies.get(TOKEN);
  
//     return !!token; // Return true if the token is present, false otherwise
//   };
  
const isLoggedIn = () => {
    const token = Cookies.get(TOKEN)

    if (token == undefined) {
        return false
    }
    return true
}

const isAdminRole = () => {
    const role = Cookies.get(ROLE)

    if (role == "ADMIN") {
        return true        
    }
    return false
}


export const AuthContext = createContext(null);
// export const AuthAdmin = createContext (null);



export const AuthProvider = ({children}) => {
    const [isAuth, setIsAuth] = useState(isLoggedIn())
    const [isAdmin, setAdmin] = useState(isAdminRole())

    const AuthLogout = () => {
        setIsAuth(false)
        setAdmin(false)
        Cookies.remove(TOKEN)
        Cookies.remove(ROLE)
    };

    // const AuthLoginAdmin = (role) => {

    //     if (role == "ADMIN") {
    //             setAdmin(true);
    //     }
    // }

    const AuthLogin = (role) => {
        setIsAuth(true);
        // if (role == "USER"){
        //     setAdmin(false)
        // }
        if (role == "ADMIN") {
            setAdmin(true);
        }else{
            setAdmin(false);
        }
    }
    return(
    <>
        {/* <AuthAdmin.Provider
        value={{
            isAdmin,
            AuthLoginAdmin,
            token: Cookies.get(TOKEN)
        }}>
            {children}
        </AuthAdmin.Provider> */}
        <AuthContext.Provider
        value={{
            isAdmin,
            isAuth,
            AuthLogout,
            AuthLogin,
            token: Cookies.get(TOKEN),
            role: Cookies.get(ROLE)
        }}
        >
            {children}
        </AuthContext.Provider>
    </>
    )
}


